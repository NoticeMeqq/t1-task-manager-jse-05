package ru.t1.annenkovsv.tm.constant;

public final class TerminalConstant {

    public static final String HELP = "HELP";

    public static final String VERSION = "VERSION";

    public static final String ABOUT = "ABOUT";

    public static final String CURRENTVERSION = "1.2.0";

    public static final String LASTUPDATED = "06.05.2022";

    public static final String DEVELOPERNAME = "Sergey Annenkov";

    public static final String EMAIL = "sannenkov@t1-consulting.ru";

    private TerminalConstant() {
    }

}
